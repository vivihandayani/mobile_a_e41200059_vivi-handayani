import 'package:flutter/material.dart';
import 'package:routing_navigation/routes.dart';

void main() {
  runApp(MaterialApp(
    onGenerateRoute: RouteGenerator.generateRoute,
  ));
}
