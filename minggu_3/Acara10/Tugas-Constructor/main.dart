import 'employee.dart';

void main() {
  var emp1 = new Employee.id(41200059);
  var emp2 = new Employee.name("Vivi Handayani");
  var emp3 = new Employee.department("Technology");

  print(emp1.id);
  print(emp2.name);
  print(emp3.department);
}
